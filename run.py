
import pathlib
import argparse

from db_base import engine, checking_pg_schemas
from license import Base as LicenseBase
from license import Parser as LicenseParser
from certificate import Base as CertificateBase
from certificate import Parser as CertificateParser

if __name__ == '__main__':
	ap = argparse.ArgumentParser()
	ap.add_argument( '--schema_create', default=False, action='store_true' )
	ap.add_argument( '--LicenseDATA', type=pathlib.Path, required=False )
	ap.add_argument( '--CertificateDATA', type=pathlib.Path, required=False )

	args = ap.parse_args()

	if args.schema_create:
		checking_pg_schemas( engine, LicenseBase.metadata )
		LicenseBase.metadata.create_all( engine )

		checking_pg_schemas( engine, CertificateBase.metadata )
		CertificateBase.metadata.create_all( engine )

	if args.LicenseDATA:
		lp = LicenseParser()
		lp.run( args.LicenseDATA )

	if args.CertificateDATA:
		cp = CertificateParser()
		cp.run( args.CertificateDATA )
