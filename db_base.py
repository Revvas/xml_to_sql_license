from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import CreateSchema


engine = create_engine( '{dialect}+{driver}://{user}:{password}@{host}:{port}/{database}'.format(
			dialect='postgresql',
			driver='psycopg2',
			user='USER',  # FIXME: change
			password='PASS',  # FIXME: change
			host='127.0.0.1',
			port='5432',
			database='DB',  # FIXME: change
			) )

Session = sessionmaker( bind=engine )
session = Session()


def checking_pg_schemas( engine, metadata ):
	print()
	print( '*** checking schemas' )
	need_schemas = set()
	for table in metadata.tables.values():
		if table.schema:
			need_schemas.add( table.schema )

	for schema_name in need_schemas:
		if not engine.dialect.has_schema( engine, schema_name ):
			print( '  CREATED ', schema_name )
			engine.execute( CreateSchema( schema_name ) )
		else:
			print( '   EXISTS ', schema_name )
	print()
